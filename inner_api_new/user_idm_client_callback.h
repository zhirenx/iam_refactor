/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef USER_IDM_CLIENT_CALLBACK_H
#define USER_IDM_CLIENT_CALLBACK_H

#include "attributes.h"
#include "iam_common_defines.h"
#include "user_idm_client_defines.h"

namespace OHOS {
namespace UserIam {
namespace UserAuth {
class GetCredentialInfoCallback {
public:
    virtual void OnCredentialInfo(const std::vector<CredentialInfo> &infoList) = 0;
};

class GetUserInfoCallback {
public:
    virtual void OnUserInfo(const UserInfo &info) = 0;
};

class UserIdmClientCallback {
public:
    virtual void OnResult(int32_t result, const RequestResult &extra) = 0;
    virtual void OnAcquireInfo(int32_t module, int32_t acquire, const RequestResult &extra) = 0;
};
} // namespace UserAuth
} // namespace UserIam
} // namespace OHOS

#endif // USER_IDM_CLIENT_CALLBACK_H