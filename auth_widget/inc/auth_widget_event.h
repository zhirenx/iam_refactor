/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_WIDGET_EVENT_H
#define AUTH_WIDGET_EVENT_H

#include <cstdint>
#include <memory>
#include <string>

#include "nocopyable.h"

#include "common.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

enum AuthWidgetEvent {
    EVENT_AUTH_TYPE_READY,
    EVENT_AUTH_TYPE_CANCEL,
    EVENT_AUTH_TYPE_CANCAL_ALL,
    EVENT_KEY_DERIVATION_READY = 10000,
};

class IAuthWidgetEvent : public NoCopyable {

public:
    explicit IAuthWidgetEvent(AuthWidgetEvent event);
    ~IAuthWidgetEvent() override = default;
    AuthWidgetEvent GetAuthWidgetEventType();
    [[nodiscard]] virtual std::string Serialize() const = 0;

private:
    AuthWidgetEvent event_;
};

class EventAuthTypeReady : public IAuthWidgetEvent {
public:
    explicit EventAuthTypeReady(AuthType type);
    static std::shared_ptr<EventAuthTypeReady> As(const std::string json);
    [[nodiscard]] std::string Serialize() const override;
};

class EventAuthTypeCancel : public IAuthWidgetEvent {
public:
    explicit EventAuthTypeCancel(AuthType type);
    static std::shared_ptr<EventAuthTypeCancel> As(const std::string json);
    [[nodiscard]] std::string Serialize() const override;
};

class EventAuthTypeCancelAll : public IAuthWidgetEvent {
public:
    explicit EventAuthTypeCancelAll();
    static std::shared_ptr<EventAuthTypeCancelAll> As(const std::string json);
    [[nodiscard]] std::string Serialize() const override;
};

class EventKeyDerivationReady : public IAuthWidgetEvent {
public:
    explicit EventKeyDerivationReady(bool success, std::string derivation);
    static std::shared_ptr<EventKeyDerivationReady> As(const std::string json);
    [[nodiscard]] std::string Serialize() const override;
};

template <typename T>
inline std::shared_ptr<T> WidgetEventCast(const std::shared_ptr<IAuthWidgetEvent> &event)
{
    return T::As(event->Serialize());
}

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS

#endif // AUTH_WIDGET_EVENT_H