/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_WIDGET_COMMON_H
#define AUTH_WIDGET_COMMON_H

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

enum AuthType {
    PIN = 1,
    FACE = 2,
    FINGERPRINT = 4,
};

enum PinSubType {
    PIN_SIX = 1,
    PIN_NUMBER = 2,
    PIN_MIX = 3,
    PIN_INVALID = 1000,
};

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS

#endif // AUTH_WIDGET_COMMON_H
