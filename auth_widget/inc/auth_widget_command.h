/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_WIDGET_COMMAND_H
#define AUTH_WIDGET_COMMAND_H

#include <cstdint>
#include <string>

#include "nocopyable.h"

#include "common.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

enum AuthWidgetCommand {
    COMMAND_NOTIFY_AUTH_START,
    COMMAND_NOTIFY_AUTH_FAILED,
    COMMAND_NOTIFY_AUTH_SUCCESS,
    COMMAND_NOTIFY_AUTH_TIMES_LEFT,
    COMMAND_NOTIFY_AUTH_FREEZE,
    COMMAND_GET_KEY_DERIVATION_DATA = 10000,
};

class IAuthWidgetCommand : public NoCopyable {

public:
    explicit IAuthWidgetCommand(AuthWidgetCommand command);
    ~IAuthWidgetCommand() override = default;
    AuthWidgetCommand GetAuthWidgetCommandType();
    [[nodiscard]] virtual std::string Serialize() const = 0;

private:
    AuthWidgetCommand command_;
};

class CommandNotifyAuthStart : public IAuthWidgetCommand {
    explicit CommandNotifyAuthStart(AuthType authType);
    [[nodiscard]] std::string Serialize() const override;
};

class CommandNotifyAuthFailed : public IAuthWidgetCommand {
    explicit CommandNotifyAuthFailed(AuthType authType);
    [[nodiscard]] std::string Serialize() const override;
};

class CommandNotifyAuthSuccess : public IAuthWidgetCommand {
    explicit CommandNotifyAuthSuccess(AuthType authType);
    [[nodiscard]] std::string Serialize() const override;
};

class CommandNotifyAuthTimesLeft : public IAuthWidgetCommand {
    explicit CommandNotifyAuthTimesLeft(AuthType authType, uint32_t times);
    [[nodiscard]] std::string Serialize() const override;
};

class CommandNotifyAuthFreeze : public IAuthWidgetCommand {
    explicit CommandNotifyAuthFreeze(AuthType authType, uint32_t second);
    [[nodiscard]] std::string Serialize() const override;
};

class CommandGetKeyDerivationData : public IAuthWidgetCommand {
    explicit CommandGetKeyDerivationData(AuthType authType, const std::string &kfdPara);
    [[nodiscard]] std::string Serialize() const override;
};

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS

#endif // AUTH_WIDGET_COMMAND_H