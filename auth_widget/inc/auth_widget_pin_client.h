/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_WIDGET_PIN_CLIENT_H
#define AUTH_WIDGET_PIN_CLIENT_H

#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "nocopyable.h"
#include "singleton.h"

#include "common.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

enum KeyDervationFunctionType {
    SCRYPT = 10000,
};

class KeyDervationFunction final : public NoCopyable {
public:
    explicit KeyDervationFunction(KeyDervationFunctionType type);
    KeyDervationFunction &SetSalt(const std::vector<uint8_t> &salt);
    KeyDervationFunction &SetDesiredKeyLength(uint32_t bytes);
    KeyDervationFunction &SetScryptN(uint32_t n);
    KeyDervationFunction &SetScryptR(uint32_t r);
    KeyDervationFunction &SetScryptP(uint32_t p);
};

class AuthWidgetPinClient final : public Singleton<AuthWidgetPinClient> {
public:
    bool GetKeyKeyDervation(uint64_t widgetHandleId, const KeyDervationFunction &kdf, PinSubType &PinSubType,
        std::vector<uint8_t> &derivedKey);
    const std::vector<KeyDervationFunctionType> &GetSupportedKeyDervationFunctionTypes();
    const std::vector<PinSubType> &GetSupportedPinSubTypes();
};

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS

#endif // AUTH_WIDGET_PIN_CLIENT_H