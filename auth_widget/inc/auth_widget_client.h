/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_WIDGET_CLIENT_H
#define AUTH_WIDGET_CLIENT_H

#include <cstdint>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "nocopyable.h"
#include "singleton.h"

#include "auth_widget_command.h"
#include "auth_widget_event.h"
#include "common.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

using AuthWidgetType = std::vector<AuthType>;
using AuthWidgetCallback = std::function<void(const std::shared_ptr<IAuthWidgetEvent> event)>;

class AuthWidgetConfig final : public NoCopyable {
public:
    explicit AuthWidgetConfig(const AuthWidgetType &authWidgetType);
    AuthWidgetConfig &SetTitle(const std::string &title);
    AuthWidgetConfig &SetTitleId(const std::string &titleId);
    AuthWidgetConfig &SetCancelButton(const std::string &cancelButton);
    AuthWidgetConfig &SetCancelButtonId(const std::string &cancelButtonId);
    AuthWidgetConfig &SetPinSubType(PinSubType pinSubType);
    AuthWidgetConfig &AddAuthWidgetCommand(const IAuthWidgetCommand &cmd);

    [[nodiscard]] std::optional<std::string> BuildConfigString() const;

private:
    std::string config_;
    std::string title_;
    std::string titleId_;
    std::string cancelButton_;
    std::string cancelButtonId_;
    const AuthWidgetType authWidgetType_ {};
    PinSubType pinSubType_ {PIN_INVALID};
};

class IAuthWidgetScheduleNode {
    virtual uint64_t GetWidgetHandleId() = 0;
    virtual bool Start(const AuthWidgetConfig &config) = 0;
    virtual bool Notify() = 0;
    virtual bool Stop() = 0;
};

class AuthWidgetClient final : public Singleton<AuthWidgetClient> {
public:
    std::shared_ptr<IAuthWidgetScheduleNode> CreateAuthWidgetScheduleNode(const AuthWidgetCallback callback);
    const std::vector<AuthWidgetType> &GetSupportedAuthWidgetTypes();
    const std::vector<AuthWidgetType> &GetSupportedAuthWidgetTypes(const AuthWidgetType &filter);
    const std::vector<PinSubType> &GetSupportedAuthWidgetPinSubTypes();
};

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS

#endif // AUTH_WIDGET_CLIENT_H