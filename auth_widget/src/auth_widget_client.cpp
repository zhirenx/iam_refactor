/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iam_logger.h"

#include "auth_widget_client.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

#define LOG_LABEL Common::LABEL_IAM_COMMON

AuthWidgetConfig::AuthWidgetConfig(const AuthWidgetType &authWidgetType) : authWidgetType_(authWidgetType)
{
}

AuthWidgetConfig &AuthWidgetConfig::SetTitle(const std::string &title)
{
    title_ = title;
    return *this;
}

AuthWidgetConfig &AuthWidgetConfig::SetTitleId(const std::string &titleId)
{
    titleId_ = titleId;
    return *this;
}

AuthWidgetConfig &AuthWidgetConfig::SetCancelButton(const std::string &cancelButton)
{
    cancelButton_ = cancelButton;

    return *this;
}

AuthWidgetConfig &AuthWidgetConfig::SetCancelButtonId(const std::string &cancelButtonId)
{
    cancelButtonId_ = cancelButtonId;
    return *this;
}

AuthWidgetConfig &AuthWidgetConfig::SetPinSubType(PinSubType pinSubType)
{
    pinSubType_ = pinSubType;
    return *this;
}

AuthWidgetConfig &AuthWidgetConfig::AddAuthWidgetCommand(const IAuthWidgetCommand &cmd)
{
    IAM_LOGD("%{public}s", cmd.Serialize().c_str());
    return *this;
}

std::optional<std::string> AuthWidgetConfig::BuildConfigString() const
{
    if (authWidgetType_.empty()) {
        return std::nullopt;
    }
    return "ssss";
}

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS
