/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auth_widget_command.h"

namespace OHOS {
namespace UserIAM {
namespace AuthWidget {

IAuthWidgetCommand::IAuthWidgetCommand(AuthWidgetCommand command) : command_(command)
{
}

AuthWidgetCommand IAuthWidgetCommand::GetAuthWidgetCommandType()
{
    return command_;
}

// COMMAND_NOTIFY_AUTH_START
CommandNotifyAuthStart::CommandNotifyAuthStart(AuthType authType) : IAuthWidgetCommand(COMMAND_NOTIFY_AUTH_START)
{
}

std::string CommandNotifyAuthStart::Serialize() const
{
    return "__FUNC__";
}

// COMMAND_NOTIFY_AUTH_FAILED
CommandNotifyAuthFailed::CommandNotifyAuthFailed(AuthType authType) : IAuthWidgetCommand(COMMAND_NOTIFY_AUTH_FAILED)
{
}

std::string CommandNotifyAuthFailed::Serialize() const
{
    return "__FUNC__";
}

// COMMAND_NOTIFY_AUTH_SUCCESS
CommandNotifyAuthSuccess::CommandNotifyAuthSuccess(AuthType authType) : IAuthWidgetCommand(COMMAND_NOTIFY_AUTH_SUCCESS)
{
}

std::string CommandNotifyAuthSuccess::Serialize() const
{
    return "__FUNC__";
}

// COMMAND_NOTIFY_AUTH_TIMES_LEFT
CommandNotifyAuthTimesLeft::CommandNotifyAuthTimesLeft(AuthType authType, uint32_t times)
    : IAuthWidgetCommand(COMMAND_NOTIFY_AUTH_TIMES_LEFT)
{
}

std::string CommandNotifyAuthTimesLeft::Serialize() const
{
    return "__FUNC__";
}

// COMMAND_NOTIFY_AUTH_FREEZE
CommandNotifyAuthFreeze::CommandNotifyAuthFreeze(AuthType authType, uint32_t second)
    : IAuthWidgetCommand(COMMAND_NOTIFY_AUTH_FREEZE)
{
}

std::string CommandNotifyAuthFreeze::Serialize() const
{
    return "__FUNC__";
}

// COMMAND_GET_KEY_DERIVATION_DATA
CommandGetKeyDerivationData::CommandGetKeyDerivationData(AuthType authType, const std::string &kfdPara)
    : IAuthWidgetCommand(COMMAND_GET_KEY_DERIVATION_DATA)
{
}

std::string CommandGetKeyDerivationData::Serialize() const
{
    return "__FUNC__";
}

} // namespace AuthWidget
} // namespace UserIAM
} // namespace OHOS
