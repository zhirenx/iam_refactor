/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "v1_0/user_auth_interface_stub.h"
#include <hdf_base.h>
#include <hdf_log.h>
#include <hdi_support.h>
#include <securec.h>
#include <string_ex.h>

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {
sptr<IUserAuthInterface> IUserAuthInterface::Get(bool isStub)
{
    return IUserAuthInterface::Get("user_auth_interface_service", isStub);
}

sptr<IUserAuthInterface> IUserAuthInterface::Get(const std::string& serviceName, bool isStub)
{
    return {};
}

UserAuthInterfaceStub::UserAuthInterfaceStub(const sptr<IUserAuthInterface> &impl)
    : IPCObjectStub(IUserAuthInterface::GetDescriptor()), impl_(impl)
{
}

UserAuthInterfaceStub::~UserAuthInterfaceStub()
{
    ObjectCollector::GetInstance().RemoveObject(impl_);
}

int32_t UserAuthInterfaceStub::OnRemoteRequest(uint32_t code, MessageParcel& data, MessageParcel& reply, MessageOption& option)
{
    switch (code) {
        case CMD_USER_AUTH_INTERFACE_INIT:
            return UserAuthInterfaceStubInit(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_ADD_EXECUTOR:
            return UserAuthInterfaceStubAddExecutor(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_DELETE_EXECUTOR:
            return UserAuthInterfaceStubDeleteExecutor(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_OPEN_SESSION:
            return UserAuthInterfaceStubOpenSession(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_CLOSE_SESSION:
            return UserAuthInterfaceStubCloseSession(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_BEGIN_ENROLLMENT:
            return UserAuthInterfaceStubBeginEnrollment(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_UPDATE_ENROLLMENT_RESULT:
            return UserAuthInterfaceStubUpdateEnrollmentResult(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_CANCEL_ENROLLMENT:
            return UserAuthInterfaceStubCancelEnrollment(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_DELETE_CREDENTIAL:
            return UserAuthInterfaceStubDeleteCredential(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_GET_CREDENTIAL:
            return UserAuthInterfaceStubGetCredential(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_GET_USER_INFO:
            return UserAuthInterfaceStubGetUserInfo(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_DELETE_USER:
            return UserAuthInterfaceStubDeleteUser(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_ENFORCE_DELETE_USER:
            return UserAuthInterfaceStubEnforceDeleteUser(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_BEGIN_AUTHENTICATION:
            return UserAuthInterfaceStubBeginAuthentication(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_UPDATE_AUTHENTICATION_RESULT:
            return UserAuthInterfaceStubUpdateAuthenticationResult(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_CANCEL_AUTHENTICATION:
            return UserAuthInterfaceStubCancelAuthentication(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_BEGIN_IDENTIFICATION:
            return UserAuthInterfaceStubBeginIdentification(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_UPDATE_IDENTIFICATION_RESULT:
            return UserAuthInterfaceStubUpdateIdentificationResult(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_CANCEL_IDENTIFICATION:
            return UserAuthInterfaceStubCancelIdentification(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_GET_AUTH_TRUST_LEVEL:
            return UserAuthInterfaceStubGetAuthTrustLevel(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_GET_VALID_SOLUTION:
            return UserAuthInterfaceStubGetValidSolution(data, reply, option);
        case CMD_USER_AUTH_INTERFACE_GET_VERSION:
            return UserAuthInterfaceStubGetVersion(data, reply, option);
        default: {
            HDF_LOGE("%{public}s: cmd %{public}d is not supported", __func__, code);
            return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
        }
    }
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubInit(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = impl_->Init();
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubAddExecutor(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    ExecutorRegisterInfo info;
    if (!ExecutorRegisterInfoBlockUnmarshalling(userAuthInterfaceData, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t index = 0;

    std::vector<uint8_t> publicKey;
    if (userAuthInterfaceMemSet) {
        publicKey.reserve(userAuthInterfaceData.ReadUint32());
    }

    std::vector<uint64_t> templateIds;
    if (userAuthInterfaceMemSet) {
        templateIds.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->AddExecutor(info, index, publicKey, templateIds);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint64(index)) {
        HDF_LOGE("%{public}s: write index failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceReply.WriteUint32(publicKey.size())) {
        HDF_LOGE("%{public}s: write publicKey.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : publicKey) {
        if (!userAuthInterfaceReply.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    if (!userAuthInterfaceReply.WriteUint32(templateIds.size())) {
        HDF_LOGE("%{public}s: write templateIds.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : templateIds) {
        if (!userAuthInterfaceReply.WriteUint64(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubDeleteExecutor(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t index = userAuthInterfaceData.ReadUint64();

    int32_t userAuthInterfaceRet = impl_->DeleteExecutor(index);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubOpenSession(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<uint8_t> challenge;
    if (userAuthInterfaceMemSet) {
        challenge.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->OpenSession(userId, challenge);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(challenge.size())) {
        HDF_LOGE("%{public}s: write challenge.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : challenge) {
        if (!userAuthInterfaceReply.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubCloseSession(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    int32_t userAuthInterfaceRet = impl_->CloseSession(userId);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubBeginEnrollment(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<uint8_t> authToken;
    uint32_t authTokenSize = userAuthInterfaceData.ReadUint32();
    authToken.reserve(authTokenSize);
    for (uint32_t i0 = 0; i0 < authTokenSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        authToken.push_back(value0);
    }

    EnrollParam param;
    if (!EnrollParamBlockUnmarshalling(userAuthInterfaceData, param)) {
        HDF_LOGE("%{public}s: read param failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    ScheduleInfo info;

    int32_t userAuthInterfaceRet = impl_->BeginEnrollment(userId, authToken, param, info);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!ScheduleInfoBlockMarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubUpdateEnrollmentResult(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<uint8_t> scheduleResult;
    uint32_t scheduleResultSize = userAuthInterfaceData.ReadUint32();
    scheduleResult.reserve(scheduleResultSize);
    for (uint32_t i0 = 0; i0 < scheduleResultSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        scheduleResult.push_back(value0);
    }

    EnrollResultInfo info;

    int32_t userAuthInterfaceRet = impl_->UpdateEnrollmentResult(userId, scheduleResult, info);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!EnrollResultInfoBlockMarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubCancelEnrollment(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    int32_t userAuthInterfaceRet = impl_->CancelEnrollment(userId);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubDeleteCredential(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    uint64_t credentialId = userAuthInterfaceData.ReadUint64();

    std::vector<uint8_t> authToken;
    uint32_t authTokenSize = userAuthInterfaceData.ReadUint32();
    authToken.reserve(authTokenSize);
    for (uint32_t i0 = 0; i0 < authTokenSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        authToken.push_back(value0);
    }

    CredentialInfo info;

    int32_t userAuthInterfaceRet = impl_->DeleteCredential(userId, credentialId, authToken, info);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!CredentialInfoBlockMarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubGetCredential(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    AuthType authType = (AuthType)userAuthInterfaceData.ReadUint32();

    std::vector<CredentialInfo> infos;
    if (userAuthInterfaceMemSet) {
        infos.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->GetCredential(userId, authType, infos);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(infos.size())) {
        HDF_LOGE("%{public}s: write infos.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : infos) {
        if (!CredentialInfoBlockMarshalling(userAuthInterfaceReply, it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubGetUserInfo(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    uint64_t secureUid = 0;

    PinSubType pinSubType;

    std::vector<EnrolledInfo> infos;
    if (userAuthInterfaceMemSet) {
        infos.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->GetUserInfo(userId, secureUid, pinSubType, infos);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint64(secureUid)) {
        HDF_LOGE("%{public}s: write secureUid failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceReply.WriteUint32((uint32_t)pinSubType)) {
        HDF_LOGE("%{public}s: write pinSubType failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceReply.WriteUint32(infos.size())) {
        HDF_LOGE("%{public}s: write infos.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : infos) {
        if (!EnrolledInfoBlockMarshalling(userAuthInterfaceReply, it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubDeleteUser(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<uint8_t> authToken;
    uint32_t authTokenSize = userAuthInterfaceData.ReadUint32();
    authToken.reserve(authTokenSize);
    for (uint32_t i0 = 0; i0 < authTokenSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        authToken.push_back(value0);
    }

    std::vector<CredentialInfo> deletedInfos;
    if (userAuthInterfaceMemSet) {
        deletedInfos.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->DeleteUser(userId, authToken, deletedInfos);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(deletedInfos.size())) {
        HDF_LOGE("%{public}s: write deletedInfos.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : deletedInfos) {
        if (!CredentialInfoBlockMarshalling(userAuthInterfaceReply, it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubEnforceDeleteUser(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<CredentialInfo> deletedInfos;
    if (userAuthInterfaceMemSet) {
        deletedInfos.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->EnforceDeleteUser(userId, deletedInfos);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(deletedInfos.size())) {
        HDF_LOGE("%{public}s: write deletedInfos.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : deletedInfos) {
        if (!CredentialInfoBlockMarshalling(userAuthInterfaceReply, it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubBeginAuthentication(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    AuthSolution param;
    if (!AuthSolutionBlockUnmarshalling(userAuthInterfaceData, param)) {
        HDF_LOGE("%{public}s: read param failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    std::vector<ScheduleInfo> scheduleInfos;
    if (userAuthInterfaceMemSet) {
        scheduleInfos.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->BeginAuthentication(contextId, param, scheduleInfos);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(scheduleInfos.size())) {
        HDF_LOGE("%{public}s: write scheduleInfos.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : scheduleInfos) {
        if (!ScheduleInfoBlockMarshalling(userAuthInterfaceReply, it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubUpdateAuthenticationResult(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    std::vector<uint8_t> scheduleResult;
    uint32_t scheduleResultSize = userAuthInterfaceData.ReadUint32();
    scheduleResult.reserve(scheduleResultSize);
    for (uint32_t i0 = 0; i0 < scheduleResultSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        scheduleResult.push_back(value0);
    }

    AuthResultInfo info;

    int32_t userAuthInterfaceRet = impl_->UpdateAuthenticationResult(contextId, scheduleResult, info);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!AuthResultInfoBlockMarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubCancelAuthentication(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    int32_t userAuthInterfaceRet = impl_->CancelAuthentication(contextId);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubBeginIdentification(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    AuthType authType = (AuthType)userAuthInterfaceData.ReadUint32();

    std::vector<uint8_t> challenge;
    uint32_t challengeSize = userAuthInterfaceData.ReadUint32();
    challenge.reserve(challengeSize);
    for (uint32_t i0 = 0; i0 < challengeSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        challenge.push_back(value0);
    }

    uint32_t executorSensorHint = userAuthInterfaceData.ReadUint32();

    ScheduleInfo scheduleInfo;

    int32_t userAuthInterfaceRet = impl_->BeginIdentification(contextId, authType, challenge, executorSensorHint, scheduleInfo);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!ScheduleInfoBlockMarshalling(userAuthInterfaceReply, scheduleInfo)) {
        HDF_LOGE("%{public}s: write scheduleInfo failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubUpdateIdentificationResult(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    std::vector<uint8_t> scheduleResult;
    uint32_t scheduleResultSize = userAuthInterfaceData.ReadUint32();
    scheduleResult.reserve(scheduleResultSize);
    for (uint32_t i0 = 0; i0 < scheduleResultSize; ++i0) {
        uint8_t value0 = userAuthInterfaceData.ReadUint8();
        scheduleResult.push_back(value0);
    }

    IdentifyResultInfo info;

    int32_t userAuthInterfaceRet = impl_->UpdateIdentificationResult(contextId, scheduleResult, info);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!IdentifyResultInfoBlockMarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubCancelIdentification(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint64_t contextId = userAuthInterfaceData.ReadUint64();

    int32_t userAuthInterfaceRet = impl_->CancelIdentification(contextId);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubGetAuthTrustLevel(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userId = userAuthInterfaceData.ReadInt32();

    AuthType authType = (AuthType)userAuthInterfaceData.ReadUint32();

    uint32_t authTrustLevel = 0;

    int32_t userAuthInterfaceRet = impl_->GetAuthTrustLevel(userId, authType, authTrustLevel);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(authTrustLevel)) {
        HDF_LOGE("%{public}s: write authTrustLevel failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubGetValidSolution(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    bool userAuthInterfaceMemSet = userAuthInterfaceData.ReadBool();

    int32_t userId = userAuthInterfaceData.ReadInt32();

    std::vector<AuthType> authTypes;
    uint32_t authTypesSize = userAuthInterfaceData.ReadUint32();
    authTypes.reserve(authTypesSize);
    for (uint32_t i0 = 0; i0 < authTypesSize; ++i0) {
        AuthType value0 = (AuthType)userAuthInterfaceData.ReadUint32();
        authTypes.push_back(value0);
    }

    uint32_t authTrustLevel = userAuthInterfaceData.ReadUint32();

    std::vector<AuthType> validTypes;
    if (userAuthInterfaceMemSet) {
        validTypes.reserve(userAuthInterfaceData.ReadUint32());
    }

    int32_t userAuthInterfaceRet = impl_->GetValidSolution(userId, authTypes, authTrustLevel, validTypes);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(validTypes.size())) {
        HDF_LOGE("%{public}s: write validTypes.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto& it0 : validTypes) {
        if (!userAuthInterfaceReply.WriteUint32((uint32_t)it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceStub::UserAuthInterfaceStubGetVersion(MessageParcel& userAuthInterfaceData, MessageParcel& userAuthInterfaceReply, MessageOption& userAuthInterfaceOption)
{
    if (userAuthInterfaceData.ReadInterfaceToken() != IUserAuthInterface::GetDescriptor()) {
        HDF_LOGE("%{public}s: interface token check failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    uint32_t majorVer = 0;

    uint32_t minorVer = 0;

    int32_t userAuthInterfaceRet = impl_->GetVersion(majorVer, minorVer);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!userAuthInterfaceReply.WriteUint32(majorVer)) {
        HDF_LOGE("%{public}s: write majorVer failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceReply.WriteUint32(minorVer)) {
        HDF_LOGE("%{public}s: write minorVer failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}
} // V1_0
} // UserAuth
} // HDI
} // OHOS
