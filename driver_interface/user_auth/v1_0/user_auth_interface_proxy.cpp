/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "v1_0/user_auth_interface_proxy.h"
#include <hdf_base.h>
#include <hdf_log.h>
#include <hdi_support.h>
#include <iservmgr_hdi.h>
#include <message_option.h>
#include <message_parcel.h>
#include <string_ex.h>

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {

sptr<IUserAuthInterface> IUserAuthInterface::Get(bool isStub)
{
    return IUserAuthInterface::Get("user_auth_interface_service", isStub);
}

sptr<IUserAuthInterface> IUserAuthInterface::Get(const std::string &serviceName, bool isStub)
{
    return {};
}

int32_t UserAuthInterfaceProxy::Init()
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_INIT, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::AddExecutor(const ExecutorRegisterInfo &info, uint64_t &index,
    std::vector<uint8_t> &publicKey, std::vector<uint64_t> &templateIds)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!ExecutorRegisterInfoBlockMarshalling(userAuthInterfaceData, info)) {
        HDF_LOGE("%{public}s: write info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_ADD_EXECUTOR, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    index = userAuthInterfaceReply.ReadUint64();

    uint32_t publicKeySize = userAuthInterfaceReply.ReadUint32();
    publicKey.reserve(publicKeySize);
    for (uint32_t i0 = 0; i0 < publicKeySize; ++i0) {
        uint8_t value0 = userAuthInterfaceReply.ReadUint8();
        publicKey.push_back(value0);
    }

    uint32_t templateIdsSize = userAuthInterfaceReply.ReadUint32();
    templateIds.reserve(templateIdsSize);
    for (uint32_t i0 = 0; i0 < templateIdsSize; ++i0) {
        uint64_t value0 = userAuthInterfaceReply.ReadUint64();
        templateIds.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::DeleteExecutor(uint64_t index)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(index)) {
        HDF_LOGE("%{public}s: write index failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_DELETE_EXECUTOR, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::OpenSession(int32_t userId, std::vector<uint8_t> &challenge)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_OPEN_SESSION, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t challengeSize = userAuthInterfaceReply.ReadUint32();
    challenge.reserve(challengeSize);
    for (uint32_t i0 = 0; i0 < challengeSize; ++i0) {
        uint8_t value0 = userAuthInterfaceReply.ReadUint8();
        challenge.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::CloseSession(int32_t userId)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_CLOSE_SESSION, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::BeginEnrollment(int32_t userId, const std::vector<uint8_t> &authToken,
    const EnrollParam &param, ScheduleInfo &info)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(authToken.size())) {
        HDF_LOGE("%{public}s: write authToken.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : authToken) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    if (!EnrollParamBlockMarshalling(userAuthInterfaceData, param)) {
        HDF_LOGE("%{public}s: write param failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_BEGIN_ENROLLMENT,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!ScheduleInfoBlockUnmarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::UpdateEnrollmentResult(int32_t userId, const std::vector<uint8_t> &scheduleResult,
    EnrollResultInfo &info)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(scheduleResult.size())) {
        HDF_LOGE("%{public}s: write scheduleResult.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : scheduleResult) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_UPDATE_ENROLLMENT_RESULT,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!EnrollResultInfoBlockUnmarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::CancelEnrollment(int32_t userId)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_CANCEL_ENROLLMENT,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::DeleteCredential(int32_t userId, uint64_t credentialId,
    const std::vector<uint8_t> &authToken, CredentialInfo &info)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(credentialId)) {
        HDF_LOGE("%{public}s: write credentialId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(authToken.size())) {
        HDF_LOGE("%{public}s: write authToken.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : authToken) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_DELETE_CREDENTIAL,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!CredentialInfoBlockUnmarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::GetCredential(int32_t userId, AuthType authType, std::vector<CredentialInfo> &infos)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32((uint32_t)authType)) {
        HDF_LOGE("%{public}s: write authType failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_GET_CREDENTIAL, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t infosSize = userAuthInterfaceReply.ReadUint32();
    infos.reserve(infosSize);
    for (uint32_t i0 = 0; i0 < infosSize; ++i0) {
        CredentialInfo value0;
        if (!CredentialInfoBlockUnmarshalling(userAuthInterfaceReply, value0)) {
            HDF_LOGE("%{public}s: read value0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
        infos.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::GetUserInfo(int32_t userId, uint64_t &secureUid, PinSubType &pinSubType,
    std::vector<EnrolledInfo> &infos)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_GET_USER_INFO, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    secureUid = userAuthInterfaceReply.ReadUint64();

    pinSubType = (PinSubType)userAuthInterfaceReply.ReadUint32();

    uint32_t infosSize = userAuthInterfaceReply.ReadUint32();
    infos.reserve(infosSize);
    for (uint32_t i0 = 0; i0 < infosSize; ++i0) {
        EnrolledInfo value0;
        if (!EnrolledInfoBlockUnmarshalling(userAuthInterfaceReply, value0)) {
            HDF_LOGE("%{public}s: read value0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
        infos.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::DeleteUser(int32_t userId, const std::vector<uint8_t> &authToken,
    std::vector<CredentialInfo> &deletedInfos)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(authToken.size())) {
        HDF_LOGE("%{public}s: write authToken.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : authToken) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_DELETE_USER, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t deletedInfosSize = userAuthInterfaceReply.ReadUint32();
    deletedInfos.reserve(deletedInfosSize);
    for (uint32_t i0 = 0; i0 < deletedInfosSize; ++i0) {
        CredentialInfo value0;
        if (!CredentialInfoBlockUnmarshalling(userAuthInterfaceReply, value0)) {
            HDF_LOGE("%{public}s: read value0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
        deletedInfos.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::EnforceDeleteUser(int32_t userId, std::vector<CredentialInfo> &deletedInfos)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_ENFORCE_DELETE_USER,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t deletedInfosSize = userAuthInterfaceReply.ReadUint32();
    deletedInfos.reserve(deletedInfosSize);
    for (uint32_t i0 = 0; i0 < deletedInfosSize; ++i0) {
        CredentialInfo value0;
        if (!CredentialInfoBlockUnmarshalling(userAuthInterfaceReply, value0)) {
            HDF_LOGE("%{public}s: read value0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
        deletedInfos.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::BeginAuthentication(uint64_t contextId, const AuthSolution &param,
    std::vector<ScheduleInfo> &scheduleInfos)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!AuthSolutionBlockMarshalling(userAuthInterfaceData, param)) {
        HDF_LOGE("%{public}s: write param failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_BEGIN_AUTHENTICATION,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t scheduleInfosSize = userAuthInterfaceReply.ReadUint32();
    scheduleInfos.reserve(scheduleInfosSize);
    for (uint32_t i0 = 0; i0 < scheduleInfosSize; ++i0) {
        ScheduleInfo value0;
        if (!ScheduleInfoBlockUnmarshalling(userAuthInterfaceReply, value0)) {
            HDF_LOGE("%{public}s: read value0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
        scheduleInfos.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::UpdateAuthenticationResult(uint64_t contextId,
    const std::vector<uint8_t> &scheduleResult, AuthResultInfo &info)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(scheduleResult.size())) {
        HDF_LOGE("%{public}s: write scheduleResult.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : scheduleResult) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_UPDATE_AUTHENTICATION_RESULT,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!AuthResultInfoBlockUnmarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::CancelAuthentication(uint64_t contextId)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_CANCEL_AUTHENTICATION,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::BeginIdentification(uint64_t contextId, AuthType authType,
    const std::vector<uint8_t> &challenge, uint32_t executorSensorHint, ScheduleInfo &scheduleInfo)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32((uint32_t)authType)) {
        HDF_LOGE("%{public}s: write authType failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(challenge.size())) {
        HDF_LOGE("%{public}s: write challenge.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : challenge) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    if (!userAuthInterfaceData.WriteUint32(executorSensorHint)) {
        HDF_LOGE("%{public}s: write executorSensorHint failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_BEGIN_IDENTIFICATION,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!ScheduleInfoBlockUnmarshalling(userAuthInterfaceReply, scheduleInfo)) {
        HDF_LOGE("%{public}s: read scheduleInfo failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::UpdateIdentificationResult(uint64_t contextId,
    const std::vector<uint8_t> &scheduleResult, IdentifyResultInfo &info)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(scheduleResult.size())) {
        HDF_LOGE("%{public}s: write scheduleResult.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : scheduleResult) {
        if (!userAuthInterfaceData.WriteUint8(it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_UPDATE_IDENTIFICATION_RESULT,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    if (!IdentifyResultInfoBlockUnmarshalling(userAuthInterfaceReply, info)) {
        HDF_LOGE("%{public}s: read info failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::CancelIdentification(uint64_t contextId)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint64(contextId)) {
        HDF_LOGE("%{public}s: write contextId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_CANCEL_IDENTIFICATION,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::GetAuthTrustLevel(int32_t userId, AuthType authType, uint32_t &authTrustLevel)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32((uint32_t)authType)) {
        HDF_LOGE("%{public}s: write authType failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_GET_AUTH_TRUST_LEVEL,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    authTrustLevel = userAuthInterfaceReply.ReadUint32();

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::GetValidSolution(int32_t userId, const std::vector<AuthType> &authTypes,
    uint32_t authTrustLevel, std::vector<AuthType> &validTypes)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteBool(false)) {
        HDF_LOGE("%{public}s:failed to write flag of memory setting!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteInt32(userId)) {
        HDF_LOGE("%{public}s: write userId failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    if (!userAuthInterfaceData.WriteUint32(authTypes.size())) {
        HDF_LOGE("%{public}s: write authTypes.size() failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    for (const auto &it0 : authTypes) {
        if (!userAuthInterfaceData.WriteUint32((uint32_t)it0)) {
            HDF_LOGE("%{public}s: write it0 failed!", __func__);
            return HDF_ERR_INVALID_PARAM;
        }
    }

    if (!userAuthInterfaceData.WriteUint32(authTrustLevel)) {
        HDF_LOGE("%{public}s: write authTrustLevel failed!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_GET_VALID_SOLUTION,
        userAuthInterfaceData, userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    uint32_t validTypesSize = userAuthInterfaceReply.ReadUint32();
    validTypes.reserve(validTypesSize);
    for (uint32_t i0 = 0; i0 < validTypesSize; ++i0) {
        AuthType value0 = (AuthType)userAuthInterfaceReply.ReadUint32();
        validTypes.push_back(value0);
    }

    return userAuthInterfaceRet;
}

int32_t UserAuthInterfaceProxy::GetVersion(uint32_t &majorVer, uint32_t &minorVer)
{
    MessageParcel userAuthInterfaceData;
    MessageParcel userAuthInterfaceReply;
    MessageOption userAuthInterfaceOption(MessageOption::TF_SYNC);

    if (!userAuthInterfaceData.WriteInterfaceToken(IUserAuthInterface::GetDescriptor())) {
        HDF_LOGE("%{public}s: failed to write interface descriptor!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    int32_t userAuthInterfaceRet = Remote()->SendRequest(CMD_USER_AUTH_INTERFACE_GET_VERSION, userAuthInterfaceData,
        userAuthInterfaceReply, userAuthInterfaceOption);
    if (userAuthInterfaceRet != HDF_SUCCESS) {
        HDF_LOGE("%{public}s failed, error code is %{public}d", __func__, userAuthInterfaceRet);
        return userAuthInterfaceRet;
    }

    majorVer = userAuthInterfaceReply.ReadUint32();

    minorVer = userAuthInterfaceReply.ReadUint32();

    return userAuthInterfaceRet;
}

} // namespace V1_0
} // namespace UserAuth
} // namespace HDI
} // namespace OHOS
