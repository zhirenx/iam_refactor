/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "v1_0/user_auth_interface_service.h"
#include <hdf_base.h>

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {
extern "C" IUserAuthInterface *UserAuthInterfaceImplGetInstance(void)
{
    return new (std::nothrow) UserAuthInterfaceService();
}

int32_t UserAuthInterfaceService::Init()
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::AddExecutor(const ExecutorRegisterInfo& info, uint64_t& index,
     std::vector<uint8_t>& publicKey, std::vector<uint64_t>& templateIds)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::DeleteExecutor(uint64_t index)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::OpenSession(int32_t userId, std::vector<uint8_t>& challenge)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::CloseSession(int32_t userId)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::BeginEnrollment(int32_t userId, const std::vector<uint8_t>& authToken,
     const EnrollParam& param, ScheduleInfo& info)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::UpdateEnrollmentResult(int32_t userId, const std::vector<uint8_t>& scheduleResult,
     EnrollResultInfo& info)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::CancelEnrollment(int32_t userId)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::DeleteCredential(int32_t userId, uint64_t credentialId,
     const std::vector<uint8_t>& authToken, CredentialInfo& info)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::GetCredential(int32_t userId, AuthType authType, std::vector<CredentialInfo>& infos)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::GetUserInfo(int32_t userId, uint64_t& secureUid, PinSubType& pinSubType,
     std::vector<EnrolledInfo>& infos)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::DeleteUser(int32_t userId, const std::vector<uint8_t>& authToken,
     std::vector<CredentialInfo>& deletedInfos)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::EnforceDeleteUser(int32_t userId, std::vector<CredentialInfo>& deletedInfos)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::BeginAuthentication(uint64_t contextId, const AuthSolution& param,
     std::vector<ScheduleInfo>& scheduleInfos)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::UpdateAuthenticationResult(uint64_t contextId,
     const std::vector<uint8_t>& scheduleResult, AuthResultInfo& info)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::CancelAuthentication(uint64_t contextId)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::BeginIdentification(uint64_t contextId, AuthType authType,
     const std::vector<uint8_t>& challenge, uint32_t executorSensorHint, ScheduleInfo& scheduleInfo)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::UpdateIdentificationResult(uint64_t contextId,
     const std::vector<uint8_t>& scheduleResult, IdentifyResultInfo& info)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::CancelIdentification(uint64_t contextId)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::GetAuthTrustLevel(int32_t userId, AuthType authType, uint32_t& authTrustLevel)
{
    return HDF_SUCCESS;
}

int32_t UserAuthInterfaceService::GetValidSolution(int32_t userId, const std::vector<AuthType>& authTypes,
     uint32_t authTrustLevel, std::vector<AuthType>& validTypes)
{
    return HDF_SUCCESS;
}
} // V1_0
} // UserAuth
} // HDI
} // OHOS
