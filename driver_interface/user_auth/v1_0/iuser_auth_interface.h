/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_HDI_USER_AUTH_V1_0_IUSERAUTHINTERFACE_H
#define OHOS_HDI_USER_AUTH_V1_0_IUSERAUTHINTERFACE_H

#include <stdint.h>
#include <string>
#include <vector>
#include <ashmem.h>
#include <hdf_base.h>
#include <hdi_base.h>
#include "user_auth/v1_0/user_auth_types.h"

#define IUSER_AUTH_INTERFACE_MAJOR_VERSION 1
#define IUSER_AUTH_INTERFACE_MINOR_VERSION 0

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {
using namespace OHOS;
using namespace OHOS::HDI;

enum {
    CMD_USER_AUTH_INTERFACE_INIT,
    CMD_USER_AUTH_INTERFACE_ADD_EXECUTOR,
    CMD_USER_AUTH_INTERFACE_DELETE_EXECUTOR,
    CMD_USER_AUTH_INTERFACE_OPEN_SESSION,
    CMD_USER_AUTH_INTERFACE_CLOSE_SESSION,
    CMD_USER_AUTH_INTERFACE_BEGIN_ENROLLMENT,
    CMD_USER_AUTH_INTERFACE_UPDATE_ENROLLMENT_RESULT,
    CMD_USER_AUTH_INTERFACE_CANCEL_ENROLLMENT,
    CMD_USER_AUTH_INTERFACE_DELETE_CREDENTIAL,
    CMD_USER_AUTH_INTERFACE_GET_CREDENTIAL,
    CMD_USER_AUTH_INTERFACE_GET_USER_INFO,
    CMD_USER_AUTH_INTERFACE_DELETE_USER,
    CMD_USER_AUTH_INTERFACE_ENFORCE_DELETE_USER,
    CMD_USER_AUTH_INTERFACE_BEGIN_AUTHENTICATION,
    CMD_USER_AUTH_INTERFACE_UPDATE_AUTHENTICATION_RESULT,
    CMD_USER_AUTH_INTERFACE_CANCEL_AUTHENTICATION,
    CMD_USER_AUTH_INTERFACE_BEGIN_IDENTIFICATION,
    CMD_USER_AUTH_INTERFACE_UPDATE_IDENTIFICATION_RESULT,
    CMD_USER_AUTH_INTERFACE_CANCEL_IDENTIFICATION,
    CMD_USER_AUTH_INTERFACE_GET_AUTH_TRUST_LEVEL,
    CMD_USER_AUTH_INTERFACE_GET_VALID_SOLUTION,
    CMD_USER_AUTH_INTERFACE_GET_VERSION,
};

class IUserAuthInterface : public HdiBase {
public:
    DECLARE_HDI_DESCRIPTOR(u"ohos.hdi.user_auth.v1_0.IUserAuthInterface");

    virtual ~IUserAuthInterface() = default;

    static sptr<IUserAuthInterface> Get(bool isStub = false);
    static sptr<IUserAuthInterface> Get(const std::string &serviceName, bool isStub = false);

    virtual int32_t Init() = 0;

    virtual int32_t AddExecutor(const ExecutorRegisterInfo& info, uint64_t& index, std::vector<uint8_t>& publicKey,
         std::vector<uint64_t>& templateIds) = 0;

    virtual int32_t DeleteExecutor(uint64_t index) = 0;

    virtual int32_t OpenSession(int32_t userId, std::vector<uint8_t>& challenge) = 0;

    virtual int32_t CloseSession(int32_t userId) = 0;

    virtual int32_t BeginEnrollment(int32_t userId, const std::vector<uint8_t>& authToken, const EnrollParam& param,
         ScheduleInfo& info) = 0;

    virtual int32_t UpdateEnrollmentResult(int32_t userId, const std::vector<uint8_t>& scheduleResult,
         EnrollResultInfo& info) = 0;

    virtual int32_t CancelEnrollment(int32_t userId) = 0;

    virtual int32_t DeleteCredential(int32_t userId, uint64_t credentialId, const std::vector<uint8_t>& authToken,
         CredentialInfo& info) = 0;

    virtual int32_t GetCredential(int32_t userId, AuthType authType, std::vector<CredentialInfo>& infos) = 0;

    virtual int32_t GetUserInfo(int32_t userId, uint64_t& secureUid, PinSubType& pinSubType,
         std::vector<EnrolledInfo>& infos) = 0;

    virtual int32_t DeleteUser(int32_t userId, const std::vector<uint8_t>& authToken,
         std::vector<CredentialInfo>& deletedInfos) = 0;

    virtual int32_t EnforceDeleteUser(int32_t userId, std::vector<CredentialInfo>& deletedInfos) = 0;

    virtual int32_t BeginAuthentication(uint64_t contextId, const AuthSolution& param,
         std::vector<ScheduleInfo>& scheduleInfos) = 0;

    virtual int32_t UpdateAuthenticationResult(uint64_t contextId, const std::vector<uint8_t>& scheduleResult,
         AuthResultInfo& info) = 0;

    virtual int32_t CancelAuthentication(uint64_t contextId) = 0;

    virtual int32_t BeginIdentification(uint64_t contextId, AuthType authType, const std::vector<uint8_t>& challenge,
         uint32_t executorSensorHint, ScheduleInfo& scheduleInfo) = 0;

    virtual int32_t UpdateIdentificationResult(uint64_t contextId, const std::vector<uint8_t>& scheduleResult,
         IdentifyResultInfo& info) = 0;

    virtual int32_t CancelIdentification(uint64_t contextId) = 0;

    virtual int32_t GetAuthTrustLevel(int32_t userId, AuthType authType, uint32_t& authTrustLevel) = 0;

    virtual int32_t GetValidSolution(int32_t userId, const std::vector<AuthType>& authTypes, uint32_t authTrustLevel,
         std::vector<AuthType>& validTypes) = 0;

    virtual int32_t GetVersion(uint32_t& majorVer, uint32_t& minorVer)
    {
        majorVer = IUSER_AUTH_INTERFACE_MAJOR_VERSION;
        minorVer = IUSER_AUTH_INTERFACE_MINOR_VERSION;
        return HDF_SUCCESS;
    }
};
} // V1_0
} // UserAuth
} // HDI
} // OHOS

#endif // OHOS_HDI_USER_AUTH_V1_0_IUSERAUTHINTERFACE_H