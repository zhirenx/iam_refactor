/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_HDI_USER_AUTH_V1_0_USERAUTHTYPES_H
#define OHOS_HDI_USER_AUTH_V1_0_USERAUTHTYPES_H

#include <string>
#include <vector>
#include <ashmem.h>
#include <message_parcel.h>

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {

using namespace OHOS;

enum AuthType : int32_t {
    ALL = 0,
    PIN = 1,
    FACE = 2,
    FINGERPRINT = 4,
};

enum ExecutorRole : int32_t {
    COLLECTOR = 1,
    VERIFIER = 2,
    ALL_IN_ONE = 3,
};

enum ExecutorSecureLevel : int32_t {
    ESL0 = 0,
    ESL1 = 1,
    ESL2 = 2,
    ESL3 = 3,
};

enum PinSubType : int32_t {
    PIN_SIX = 10000,
    PIN_NUMBER = 10001,
    PIN_MIX = 10002,
};

enum ScheduleMode : int32_t {
    ENROLL = 0,
    AUTH = 1,
    IDENTIFY = 2,
};

struct ExecutorRegisterInfo {
    AuthType authType;
    ExecutorRole executorRole;
    uint32_t executorSensorHint;
    uint32_t executorMatcher;
    ExecutorSecureLevel esl;
    std::vector<uint8_t> publicKey;
};

struct ExecutorInfo {
    uint64_t executorIndex;
    ExecutorRegisterInfo info;
};

struct ScheduleInfo {
    uint64_t scheduleId;
    std::vector<uint64_t> templateIds;
    AuthType authType;
    uint32_t executorMatcher;
    ScheduleMode scheduleMode;
    std::vector<ExecutorInfo> executors;
};

struct AuthSolution {
    int32_t userId;
    uint32_t authTrustLevel;
    AuthType authType;
    uint32_t executorSensorHint;
    std::vector<uint8_t> challenge;
};

struct ExecutorSendMsg {
    uint64_t executorIndex;
    int32_t commandId;
    std::vector<uint8_t> msg;
};

struct AuthResultInfo {
    int32_t result;
    int32_t freezingTime;
    int32_t remainTimes;
    std::vector<ExecutorSendMsg> msgs;
    std::vector<uint8_t> token;
    std::vector<uint8_t> rootSecret;
};

struct IdentifyResultInfo {
    int32_t result;
    int32_t userId;
    std::vector<uint8_t> token;
};

struct EnrollParam {
    AuthType authType;
    uint32_t executorSensorHint;
};

struct CredentialInfo {
    uint64_t credentialId;
    uint64_t executorIndex;
    uint64_t templateId;
    AuthType authType;
    uint32_t executorMatcher;
    uint32_t executorSensorHint;
};

struct EnrolledInfo {
    uint64_t enrolledId;
    AuthType authType;
};

struct EnrollResultInfo {
    uint64_t credentialId;
    CredentialInfo oldInfo;
    std::vector<uint8_t> rootSecret;
};

bool ExecutorRegisterInfoBlockMarshalling(OHOS::MessageParcel &data, const ExecutorRegisterInfo& dataBlock);

bool ExecutorRegisterInfoBlockUnmarshalling(OHOS::MessageParcel &data, ExecutorRegisterInfo& dataBlock);

bool ExecutorInfoBlockMarshalling(OHOS::MessageParcel &data, const ExecutorInfo& dataBlock);

bool ExecutorInfoBlockUnmarshalling(OHOS::MessageParcel &data, ExecutorInfo& dataBlock);

bool ScheduleInfoBlockMarshalling(OHOS::MessageParcel &data, const ScheduleInfo& dataBlock);

bool ScheduleInfoBlockUnmarshalling(OHOS::MessageParcel &data, ScheduleInfo& dataBlock);

bool AuthSolutionBlockMarshalling(OHOS::MessageParcel &data, const AuthSolution& dataBlock);

bool AuthSolutionBlockUnmarshalling(OHOS::MessageParcel &data, AuthSolution& dataBlock);

bool ExecutorSendMsgBlockMarshalling(OHOS::MessageParcel &data, const ExecutorSendMsg& dataBlock);

bool ExecutorSendMsgBlockUnmarshalling(OHOS::MessageParcel &data, ExecutorSendMsg& dataBlock);

bool AuthResultInfoBlockMarshalling(OHOS::MessageParcel &data, const AuthResultInfo& dataBlock);

bool AuthResultInfoBlockUnmarshalling(OHOS::MessageParcel &data, AuthResultInfo& dataBlock);

bool IdentifyResultInfoBlockMarshalling(OHOS::MessageParcel &data, const IdentifyResultInfo& dataBlock);

bool IdentifyResultInfoBlockUnmarshalling(OHOS::MessageParcel &data, IdentifyResultInfo& dataBlock);

bool EnrollParamBlockMarshalling(OHOS::MessageParcel &data, const EnrollParam& dataBlock);

bool EnrollParamBlockUnmarshalling(OHOS::MessageParcel &data, EnrollParam& dataBlock);

bool CredentialInfoBlockMarshalling(OHOS::MessageParcel &data, const CredentialInfo& dataBlock);

bool CredentialInfoBlockUnmarshalling(OHOS::MessageParcel &data, CredentialInfo& dataBlock);

bool EnrolledInfoBlockMarshalling(OHOS::MessageParcel &data, const EnrolledInfo& dataBlock);

bool EnrolledInfoBlockUnmarshalling(OHOS::MessageParcel &data, EnrolledInfo& dataBlock);

bool EnrollResultInfoBlockMarshalling(OHOS::MessageParcel &data, const EnrollResultInfo& dataBlock);

bool EnrollResultInfoBlockUnmarshalling(OHOS::MessageParcel &data, EnrollResultInfo& dataBlock);

} // V1_0
} // UserAuth
} // HDI
} // OHOS

#endif // OHOS_HDI_USER_AUTH_V1_0_USERAUTHTYPES_H