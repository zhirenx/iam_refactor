/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_HDI_USER_AUTH_V1_0_USERAUTHINTERFACEPROXY_H
#define OHOS_HDI_USER_AUTH_V1_0_USERAUTHINTERFACEPROXY_H

#include "v1_0/iuser_auth_interface.h"
#include <iproxy_broker.h>

namespace OHOS {
namespace HDI {
namespace UserAuth {
namespace V1_0 {

class UserAuthInterfaceProxy : public IProxyBroker<IUserAuthInterface> {
public:
    explicit UserAuthInterfaceProxy(const sptr<IRemoteObject>& remote) : IProxyBroker<IUserAuthInterface>(remote) {}

    virtual ~UserAuthInterfaceProxy() = default;

    int32_t Init() override;

    int32_t AddExecutor(const ExecutorRegisterInfo& info, uint64_t& index, std::vector<uint8_t>& publicKey,
         std::vector<uint64_t>& templateIds) override;

    int32_t DeleteExecutor(uint64_t index) override;

    int32_t OpenSession(int32_t userId, std::vector<uint8_t>& challenge) override;

    int32_t CloseSession(int32_t userId) override;

    int32_t BeginEnrollment(int32_t userId, const std::vector<uint8_t>& authToken, const EnrollParam& param,
         ScheduleInfo& info) override;

    int32_t UpdateEnrollmentResult(int32_t userId, const std::vector<uint8_t>& scheduleResult,
         EnrollResultInfo& info) override;

    int32_t CancelEnrollment(int32_t userId) override;

    int32_t DeleteCredential(int32_t userId, uint64_t credentialId, const std::vector<uint8_t>& authToken,
         CredentialInfo& info) override;

    int32_t GetCredential(int32_t userId, AuthType authType, std::vector<CredentialInfo>& infos) override;

    int32_t GetUserInfo(int32_t userId, uint64_t& secureUid, PinSubType& pinSubType,
         std::vector<EnrolledInfo>& infos) override;

    int32_t DeleteUser(int32_t userId, const std::vector<uint8_t>& authToken,
         std::vector<CredentialInfo>& deletedInfos) override;

    int32_t EnforceDeleteUser(int32_t userId, std::vector<CredentialInfo>& deletedInfos) override;

    int32_t BeginAuthentication(uint64_t contextId, const AuthSolution& param,
         std::vector<ScheduleInfo>& scheduleInfos) override;

    int32_t UpdateAuthenticationResult(uint64_t contextId, const std::vector<uint8_t>& scheduleResult,
         AuthResultInfo& info) override;

    int32_t CancelAuthentication(uint64_t contextId) override;

    int32_t BeginIdentification(uint64_t contextId, AuthType authType, const std::vector<uint8_t>& challenge,
         uint32_t executorSensorHint, ScheduleInfo& scheduleInfo) override;

    int32_t UpdateIdentificationResult(uint64_t contextId, const std::vector<uint8_t>& scheduleResult,
         IdentifyResultInfo& info) override;

    int32_t CancelIdentification(uint64_t contextId) override;

    int32_t GetAuthTrustLevel(int32_t userId, AuthType authType, uint32_t& authTrustLevel) override;

    int32_t GetValidSolution(int32_t userId, const std::vector<AuthType>& authTypes, uint32_t authTrustLevel,
         std::vector<AuthType>& validTypes) override;

    int32_t GetVersion(uint32_t& majorVer, uint32_t& minorVer) override;

private:
    static inline BrokerDelegator<UserAuthInterfaceProxy> delegator_;
};

} // V1_0
} // UserAuth
} // HDI
} // OHOS

#endif // OHOS_HDI_USER_AUTH_V1_0_USERAUTHINTERFACEPROXY_H