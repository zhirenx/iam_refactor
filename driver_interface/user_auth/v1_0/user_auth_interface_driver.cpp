/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hdf_base.h>
#include <hdf_device_desc.h>
#include <hdf_log.h>
#include <hdf_sbuf_ipc.h>
#include "v1_0/user_auth_interface_stub.h"

using namespace OHOS::HDI::UserAuth::V1_0;

struct HdfUserAuthInterfaceHost {
    struct IDeviceIoService ioService;
    OHOS::sptr<OHOS::IRemoteObject> stub;
};

static int32_t UserAuthInterfaceDriverDispatch(struct HdfDeviceIoClient *client, int cmdId, struct HdfSBuf *data,
    struct HdfSBuf *reply)
{
    auto *hdfUserAuthInterfaceHost = CONTAINER_OF(client->device->service, struct HdfUserAuthInterfaceHost, ioService);

    OHOS::MessageParcel *dataParcel = nullptr;
    OHOS::MessageParcel *replyParcel = nullptr;
    OHOS::MessageOption option;

    if (SbufToParcel(data, &dataParcel) != HDF_SUCCESS) {
        HDF_LOGE("%{public}s:invalid data sbuf object to dispatch", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    if (SbufToParcel(reply, &replyParcel) != HDF_SUCCESS) {
        HDF_LOGE("%{public}s:invalid reply sbuf object to dispatch", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    return hdfUserAuthInterfaceHost->stub->SendRequest(cmdId, *dataParcel, *replyParcel, option);
}

int HdfUserAuthInterfaceDriverInit(struct HdfDeviceObject *deviceObject)
{
    HDF_LOGI("HdfUserAuthInterfaceDriverInit enter");
    return HDF_SUCCESS;
}

int HdfUserAuthInterfaceDriverBind(struct HdfDeviceObject *deviceObject)
{
    HDF_LOGI("HdfUserAuthInterfaceDriverBind enter");

    auto *hdfUserAuthInterfaceHost = new (std::nothrow) HdfUserAuthInterfaceHost;
    if (hdfUserAuthInterfaceHost == nullptr) {
        HDF_LOGE("%{public}s: failed to create create HdfUserAuthInterfaceHost object", __func__);
        return HDF_FAILURE;
    }

    hdfUserAuthInterfaceHost->ioService.Dispatch = UserAuthInterfaceDriverDispatch;
    hdfUserAuthInterfaceHost->ioService.Open = NULL;
    hdfUserAuthInterfaceHost->ioService.Release = NULL;

    auto serviceImpl = IUserAuthInterface::Get(true);
    if (serviceImpl == nullptr) {
        HDF_LOGE("%{public}s: failed to get of implement service", __func__);
        delete hdfUserAuthInterfaceHost;
        return HDF_FAILURE;
    }

    hdfUserAuthInterfaceHost->stub = OHOS::HDI::ObjectCollector::GetInstance().GetOrNewObject(serviceImpl,
        IUserAuthInterface::GetDescriptor());
    if (hdfUserAuthInterfaceHost->stub == nullptr) {
        HDF_LOGE("%{public}s: failed to get stub object", __func__);
        delete hdfUserAuthInterfaceHost;
        return HDF_FAILURE;
    }

    deviceObject->service = &hdfUserAuthInterfaceHost->ioService;
    return HDF_SUCCESS;
}

void HdfUserAuthInterfaceDriverRelease(struct HdfDeviceObject *deviceObject){
    HDF_LOGI("HdfUserAuthInterfaceDriverRelease enter");
    if (deviceObject->service == nullptr) {
        HDF_LOGE("HdfUserAuthInterfaceDriverRelease not initted");
        return;
    }

    auto *hdfUserAuthInterfaceHost = CONTAINER_OF(deviceObject->service, struct HdfUserAuthInterfaceHost, ioService);
    delete hdfUserAuthInterfaceHost;
}

struct HdfDriverEntry g_userauthinterfaceDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "drivers_peripheral_user_auth",
    .Bind = HdfUserAuthInterfaceDriverBind,
    .Init = HdfUserAuthInterfaceDriverInit,
    .Release = HdfUserAuthInterfaceDriverRelease,
};

#ifndef __cplusplus
extern "C" {
#endif
HDF_INIT(g_userauthinterfaceDriverEntry);
#ifndef __cplusplus
}
#endif
