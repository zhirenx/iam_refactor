/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "system_ability.h"

#include "datetime_ex.h"
#include "errors.h"
#include "ipc_skeleton.h"
#include "safwk_log.h"
#include "string_ex.h"
#include <cinttypes>

namespace OHOS {
namespace {
const std::string TAG = "SystemAbility";
}

SystemAbility::SystemAbility(bool runOnCreate)
{
}

SystemAbility::SystemAbility(int32_t systemAbilityId, bool runOnCreate) : SystemAbility(runOnCreate)
{
    saId_ = systemAbilityId;
}

SystemAbility::~SystemAbility()
{
    HILOGI(TAG, "SA:%{public}d destroyed", saId_);
}

bool SystemAbility::MakeAndRegisterAbility(SystemAbility *systemAbility)
{
    HILOGD(TAG, "registering system ability...");
    if (systemAbility) {
        delete systemAbility;
    }
    return true;
}

bool SystemAbility::AddSystemAbilityListener(int32_t systemAbilityId)
{
    HILOGD(TAG, "SA:%{public}d, listenerSA:%{public}d", systemAbilityId, saId_);
    return true;
}

bool SystemAbility::RemoveSystemAbilityListener(int32_t systemAbilityId)
{
    HILOGD(TAG, "SA:%{public}d, listenerSA:%{public}d", systemAbilityId, saId_);
    return true;
}

bool SystemAbility::Publish(sptr<IRemoteObject> systemAbility)
{
    return true;
}

void SystemAbility::StopAbility(int32_t systemAbilityId)
{
    return;
}

void SystemAbility::Start()
{
    HILOGD(TAG, "starting system ability...");
    if (isRunning_) {
        return;
    }
    HILOGI(TAG, "[PerformanceTest] SAFWK OnStart systemAbilityId:%{public}d", saId_);
    int64_t begin = GetTickCount();
    OnStart();
    isRunning_ = true;
    HILOGI(TAG, "[PerformanceTest] SAFWK OnStart systemAbilityId:%{public}d finished, spend:%{public}" PRId64 " ms",
        saId_, (GetTickCount() - begin));
}

void SystemAbility::Stop()
{
    HILOGD(TAG, "stopping system ability...");

    if (!isRunning_) {
        return;
    }

    OnStop();
    isRunning_ = false;
}

void SystemAbility::SADump()
{
    OnDump();
}

int32_t SystemAbility::GetSystemAbilitId() const
{
    return saId_;
}

void SystemAbility::SetLibPath(const std::u16string &libPath)
{
    libPath_ = libPath;
}

const std::u16string &SystemAbility::GetLibPath() const
{
    return libPath_;
}

void SystemAbility::SetDependSa(const std::vector<std::u16string> &dependSa)
{
    dependSa_ = dependSa;
}

const std::vector<std::u16string> &SystemAbility::GetDependSa() const
{
    return dependSa_;
}

void SystemAbility::SetRunOnCreate(bool isRunOnCreate)
{
    isRunOnCreate_ = isRunOnCreate;
}

bool SystemAbility::IsRunOnCreate() const
{
    return isRunOnCreate_;
}

void SystemAbility::SetDistributed(bool isDistributed)
{
    isDistributed_ = isDistributed;
}

bool SystemAbility::GetDistributed() const
{
    return isDistributed_;
}

bool SystemAbility::GetRunningStatus() const
{
    return isRunning_;
}

void SystemAbility::SetDumpLevel(uint32_t dumpLevel)
{
    dumpLevel_ = dumpLevel;
}

uint32_t SystemAbility::GetDumpLevel() const
{
    return dumpLevel_;
}

void SystemAbility::SetDependTimeout(int32_t dependTimeout)
{
    HILOGD(TAG, "new dependTimeout:%{public}d, original dependTimeout:%{public}d", dependTimeout, dependTimeout_);
}

int32_t SystemAbility::GetDependTimeout() const
{
    return dependTimeout_;
}

// The details should be implemented by subclass
void SystemAbility::OnDump()
{
}

// The details should be implemented by subclass
void SystemAbility::OnStart()
{
}

// The details should be implemented by subclass
void SystemAbility::OnStop()
{
}

// The details should be implemented by subclass
void SystemAbility::OnAddSystemAbility(int32_t systemAbilityId, const std::string &deviceId)
{
}

// The details should be implemented by subclass
void SystemAbility::OnRemoveSystemAbility(int32_t systemAbilityId, const std::string &deviceId)
{
}

sptr<IRemoteObject> SystemAbility::GetSystemAbility(int32_t systemAbilityId)
{
    return {};
}

void SystemAbility::SetCapability(const std::u16string &capability)
{
    capability_ = capability;
}

const std::u16string &SystemAbility::GetCapability() const
{
    return capability_;
}

void SystemAbility::SetPermission(const std::u16string &permission)
{
    permission_ = permission;
}
} // namespace OHOS
